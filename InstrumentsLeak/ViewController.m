/**
 *  @file ViewController.m
 *  InstrumentsLeak
 *  @brief Creación de un Memory Leak
 *
 *  @author Alfonso Sanchez 
 *  @copyright Copyright (c) 2012 Alfortel Telecom. @n All rights reserved.
 *  @version 1.0
 *
 */

#import "ViewController.h"

@implementation ViewController

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/**
 * @brief View Did Load
 *
 * Método al que se llama al principio, después de la carga de la vista. 
 * Crea el primer @c alloc de la variable @c primerSring.
 * Este método llama a @c crearSegundoString.
 */
- (void)viewDidLoad
{
    [super viewDidLoad];
	/** Do any additional setup after loading the view, typically from a nib.
     * Se crea el primer String reservando memoria mediante un alloc
     */
    primerString = [[NSString alloc] initWithUTF8String:"Hola, soy el primer String"];
	
    // Se llama al método crearSegundoString
	[self crearSegundoString];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}

/**
 * @brief Rotación de la vista del dispositivo.
 *
 * Devuelve un tipo BOOL para permitir que la vista rote al girar el dispositivo.
 * Sobreescribe este método para cambiar el funcionamiento.
 * 
 * @param  interfaceOrientation Orientación del dispositivo.
 * @return YES si se permite que la vista cambie al girar el dispositivo. 
 * NO, en otro caso.
 */
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

- (void) crearSegundoString
{
	// Eliminación del memory leak!!
     [primerString release];
	// Se vuelve a reservar memoria alloc sin haber hecho un release
	primerString = [[NSString alloc] initWithUTF8String: "Hola, soy el segundo String y si se ha hecho un release"];
    
}

/** 
 *  @brief Destructor
 */ 
- (void)dealloc {
    [super dealloc];
}
@end
