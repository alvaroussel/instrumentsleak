//
//  AppDelegate.h
//  InstrumentsLeak
//
//  Created by Alfonso Sanchez Valdeolivas on 19/06/12.
//  Copyright (c) 2012 Alfortel Telecomunicaciones. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
