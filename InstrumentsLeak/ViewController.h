/**
 *  @file ViewController.h
 *  InstrumentsLeak
 *  @brief Creación de un Memory Leak
 *
 *  @author Alfonso Sanchez 
 *  @copyright Copyright (c) 2012 Alfortel Telecom. @n All rights reserved.
 *  @version 1.0
 *
 * Esta clase va a crear un memory leak debido a que se hace un alloc sobre
 * un puntero que estaba apuntando a otra zona de memoria sin hacer antes un
 * release.
 */



#import <UIKit/UIKit.h>


/** 
 * Creación del puntero @c NSString
 * @category UIViewController 
 *
 */

@interface ViewController : UIViewController{
	NSString* primerString; /**< variable de ViewController. Referencia a un NSString. */
}


/**
 * @brief Creación del segundo NSString
 * Creación de una referencia a un @c NSString. La variable @c primerString 
 * va a referenciar a un @c NSString sin haber hecho antes un @c release
 *
 */
- (void) crearSegundoString;


@end